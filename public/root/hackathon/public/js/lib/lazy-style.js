define(function() {
    var lazyStyle = function() {
        this.init = function() {
            var lazyEl = document.querySelectorAll('.lazy-style'),
                lazyElLength = lazyEl.length;

            for (var i=0; i<lazyElLength; i++) {
                loadImage(lazyEl[i]);
            }

            function loadImage(container) {
                var src = container.getAttribute('data-img'),
                    handlerImage = new Image();

                handlerImage.src = src;
                handlerImage.container = container;
                handlerImage.onload = imageDone;
            };

            function imageDone() {
                this.container.style.backgroundImage = 'url('+this.src+')';
                this.container.className += ' show';
            };
        }
    }
    return new lazyStyle();
})
