require.config({
    baseUrl: "/js/",
    paths: {
        "jquery": 'lib/jquery',
        "lazy-style": "lib/lazy-style",
        "wookmark": "lib/wookmark",
        "text": "lib/text",
        "react": "lib/react",
        "JSXTransformer": "lib/JSXTransformer"
    },
    shim: {
        'JSXTransformer': {
            deps: ["react"]
        },
        'wookmark': ['jquery']
    },
    jsx: {
        fileExtension: '.js'
    }

})

require([
    'app'
], function(App) {
    App.init();
})