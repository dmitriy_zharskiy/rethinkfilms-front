module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-bg-shell');
  // ���������� grunt-reload
  grunt.loadNpmTasks('grunt-reload');

  // Project configuration.
  grunt.initConfig({
    bgShell: {
      //��������� ���������� � ������� supervisor'a
      //������ ��� ��������� ���������� ���� 
      //������ ��������������� �������������
      supervisor: {
            cmd: 'supervisor server.js',
            stdout: true,
            stderr: true,
            bg: true
      }
    },
    //����������� reload
    //������ ���������� �������� �� localhost:3000
    //��������� �� localhost:6001 � �������� �� �� ���������� ������ � LiveReload
    reload: {
      port: 3000,
      proxy: {
        host: 'http://hackathon.ngalochkin.dev/',
        port: 80
      },
    },
    watch: {
      //��� ��������� ������ �� ���� ������ ��������� ������ 'reload'
      files: [
        //add here static file which need to be livereloaded
        'public/*'
        ],
      tasks: 'reload'
    }
  });

  //�������� ����������
  //reload � �� ������� � �� �������
  grunt.registerTask('server', 'bgShell:supervisor reload watch');
};  
